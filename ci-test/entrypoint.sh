#!/usr/bin/env bash

sh -c "export PATH=\"/home/cirrus/sdks/flutter/bin/:\$PATH\""

sh -c "sudo su"
sh -c "chmod -R 777 ."

sh -c "flutter packages get"
sh -c "flutter test"

sh -c "exit $?;"
