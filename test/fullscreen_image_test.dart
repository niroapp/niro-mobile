import 'package:ecmm427mobile/screens/fullscreen_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:image_test_utils/image_test_utils.dart';

class TestButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) {
              return FullscreenImageScreen();
            },
            settings: RouteSettings(arguments: {
              'name': 'name',
              'imageURL': 'http://oi42.tinypic.com/330uxzt.jpg'
            })));
      },
    );
  }
}


void main() {
  Widget makeTestableWidget({Widget child}) {
    return MaterialApp(
      home: child,
    );
  }

  testWidgets('image', (WidgetTester tester) async {
    provideMockedNetworkImages(() async {
      await tester.pumpWidget(makeTestableWidget(child: TestButton()));

      await tester.tap(find.byType(MaterialButton));
      await tester.pumpAndSettle();
      Finder title = find.byType(Image);
      expect(title, findsOneWidget);
    });
  });
}
