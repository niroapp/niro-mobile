import 'dart:async';
import 'dart:convert';
import 'package:ecmm427mobile/internal/app_state.dart';
import 'package:ecmm427mobile/internal/app_state_container.dart';
import 'package:ecmm427mobile/internal/collection.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ecmm427mobile/screens/library_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mockito/mockito.dart';
import 'package:path/path.dart';

class MockAppState extends Mock implements AppStateContainerState {
  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.debug}) {
    return super.toString();
  }
}
void main(){
  Widget getRootWidget({Widget child}) {
    return MediaQuery(
        data: new MediaQueryData(),
        child: MaterialApp(home: AppStateContainer(child: child)));
  }

  Widget makeTestableWidget({Widget child}) {
    return MaterialApp(
      home: child,
    );
  }
  testWidgets('text', (WidgetTester tester) async {
    await tester.pumpWidget(makeTestableWidget(child: LibraryScreen()));
    final labelText = find.text('Open Source Libraries');
    expect (labelText,findsOneWidget);

  });

  testWidgets('find loading indicator if list is empty',
          (WidgetTester tester) async {
        final libraryScreen = getRootWidget(
            child: LibraryScreen()); // Get a reference to a collectionScreen
        await tester.pumpWidget(
            libraryScreen); // Populate the test with CollectionScreen

        final libraryScreenFinder = find.byWidget(
            libraryScreen); // Find the collectionScreen within the test environment
        expect(libraryScreenFinder,
            findsOneWidget); // Ensure that it's found a widget

        final loadingIconFinder = find.descendant(
            of: libraryScreenFinder,
            matching: find.byType(
                CircularProgressIndicator)); // Find all the potential child elements within CollectionScreen
        expect(loadingIconFinder, findsOneWidget);
      });


}