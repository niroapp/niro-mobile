import 'dart:async';
import 'package:ecmm427mobile/internal/app_state.dart';
import 'package:ecmm427mobile/internal/app_state_container.dart';
import 'package:ecmm427mobile/internal/result.dart';
import 'package:ecmm427mobile/screens/result_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:ecmm427mobile/widgets/list_button.dart';


class MockAppState extends Mock implements AppStateContainerState {
  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.debug}) {
    return super.toString();
  }
}
void main(){
  Widget getRootWidget({Widget child}) {
    return MediaQuery(
        data: new MediaQueryData(),
        child: MaterialApp(home: AppStateContainer(child: child)));
  }

  testWidgets('find loading indicator if list is empty', (WidgetTester tester) async {
    final resultscreen = getRootWidget(
        child: ResultScreen()); // Get a reference to a collectionScreen
    await tester.pumpWidget(
        resultscreen); // Populate the test with CollectionScreen

    final resultscreenFinder = find.byWidget(
        resultscreen); // Find the collectionScreen within the test environment
    expect(resultscreenFinder,
        findsOneWidget); // Ensure that it's found a widget

    final loadingIconFinder = find.descendant(
        of: resultscreenFinder,
        matching: find.byType(
            CircularProgressIndicator)); // Find all the potential child elements within CollectionScreen
    expect(loadingIconFinder, findsOneWidget);

  });


}