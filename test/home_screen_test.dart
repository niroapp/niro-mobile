import 'package:ecmm427mobile/internal/app_state_container.dart';
import 'package:ecmm427mobile/screens/collection_screen.dart';
import 'package:ecmm427mobile/screens/home_screen.dart';
import 'package:ecmm427mobile/screens/login_screen.dart';
import 'package:ecmm427mobile/screens/result_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {
  Widget makeTestableWidget({Widget child}) {
    return MaterialApp(home: child, routes: {
      '/CollectionScreen': (context) => CollectionScreen(),
      '/ResultScreen': (context) => ResultScreen(),
      '/LoginScreenExeter': (context) => LogInScreen(true),
    });
  }

  testWidgets('everything is succeed', (WidgetTester tester) async {
    await tester.pumpWidget(makeTestableWidget(child: HomeScreen()));
    final finder = find.text('NiRO'); //找到所有的text树
    expect(finder, findsWidgets);

    final submitbutton = find.text('Submit a new scan');
    expect(submitbutton, findsOneWidget);

    await tester.tap(submitbutton);
    //await tester.pump();
    //expect (find.text('Submit Scan'), findsOneWidget);
    expect(find.byType(Navigator), findsOneWidget);
  });

  Widget getRootWidget({Widget child, NavigatorObserver observer}) {
    return MediaQuery(
      data: new MediaQueryData(),
      child: AppStateContainer(
        child: Builder(
          builder: (context) {
            return MaterialApp(initialRoute: "/", routes: {
              "/": (context) => HomeScreen(),
              '/CollectionScreen': (context) => CollectionScreen(),
              '/ResultScreen': (context) => ResultScreen(),
              '/LoginScreenExeter': (context) => LogInScreen(true),
            }, navigatorObservers: [
              observer
            ]);
          },
        ),
      ),
    );
  }

  testWidgets('Results screen displays if that route is chosen',
          (WidgetTester tester) async {
    final mockObserver = MockNavigatorObserver();

    await tester
        .pumpWidget(getRootWidget(child: HomeScreen(), observer: mockObserver));
    final viewButton = find.text('View previous results');
    expect(viewButton, findsOneWidget);

    await tester.tap(viewButton);
    await tester.pumpAndSettle();

    verify(mockObserver.didPush(typed(any), typed(any)));
    expect(find.byType(LogInScreen), findsOneWidget);
  });
}
