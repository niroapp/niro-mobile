import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ecmm427mobile/screens/settings_screen.dart';

import 'package:ecmm427mobile/screens/library_screen.dart';
void main() {


Widget makeTestableWidget ({Widget child}){
  return MaterialApp(
    home: child,
    routes: {
      '/LibraryScreen': (context) => LibraryScreen(),
    }

  );
}
  testWidgets('everything is succeed', (WidgetTester tester) async{
    await tester.pumpWidget(makeTestableWidget(child: SettingsScreen())); //await tester.pumpWidget(SettingsScreen());
    final finder = find.text('Settings'); //find all text widget
    expect (finder,findsOneWidget);

    final materialbutton = find.byType(MaterialButton);
    expect(materialbutton, findsOneWidget);
    await tester.tap(materialbutton);
    await tester.pump();
    //连接
    expect(find.byType(Navigator), findsOneWidget);
  });
}