import 'dart:async';

import 'package:ecmm427mobile/internal/app_state.dart';
import 'package:ecmm427mobile/internal/app_state_container.dart';
import 'package:ecmm427mobile/internal/record.dart';
import 'package:ecmm427mobile/screens/record_screen.dart';
import 'package:ecmm427mobile/widgets/list_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockAppState extends Mock implements AppStateContainerState {
  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.debug}) {
    return super.toString();
  }
}

void main() {
  Widget getRootWidget({Widget child}) {
    return MediaQuery(
        data: new MediaQueryData(),
        child: MaterialApp(home: AppStateContainer(child: child)));
  }

  testWidgets('the list populates correctly', (WidgetTester tester) async {
    final screen = RecordScreen();
    final MockAppState container = MockAppState();

    await tester.pumpWidget(getRootWidget(child: screen));

    final RecordScreenState state = tester.state(find.byWidget(screen));

    final List<Record> list =
    List.of([Record.fromValues("name", "0", "ID", null)]);
    Completer completer = Completer();
    completer.complete();

    final AppState appState = AppState();
    appState.records = list;

    when(container.recordsFuture).thenAnswer((_) => Future.value(list));
    when(container.state).thenAnswer((_) => appState);

    await tester.pumpWidget(getRootWidget(child: state.getContent(container)));

    await tester.idle();
    await tester.pump();

    final listViewFinder = find.byType(ListView);
    expect(
        find.descendant(of: listViewFinder, matching: find.byType(ListButton)),
        findsOneWidget);
  });

  testWidgets('the page gives a message if the list is empty',
          (WidgetTester tester) async {
        final screen = RecordScreen();
        final MockAppState container = MockAppState();

        await tester.pumpWidget(getRootWidget(child: screen));

        final RecordScreenState state = tester.state(find.byWidget(screen));

        final List<Record> list = List();
        Completer completer = Completer();
        completer.complete();

        final AppState appState = AppState();
        appState.records = list;

        when(container.recordsFuture).thenAnswer((_) => Future.value(list));
        when(container.state).thenAnswer((_) => appState);

        await tester.pumpWidget(
            getRootWidget(child: state.getContent(container)));

        await tester.idle();
    await tester.pump();

        expect(find.text('The chosen collection is empty.'), findsOneWidget);
  });
}
