import 'dart:async';

import 'package:ecmm427mobile/internal/app_state.dart';
import 'package:ecmm427mobile/internal/app_state_container.dart';
import 'package:ecmm427mobile/internal/collection.dart';
import 'package:ecmm427mobile/screens/collection_screen.dart';
import 'package:ecmm427mobile/widgets/list_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';


class MockAppState extends Mock implements AppStateContainerState {
  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.debug}) {
    return super.toString();
  }
}


void main() {
  // Use this method to to provide the context needed to all the widgets
  Widget getRootWidget({Widget child}) {
    return MediaQuery(
        data: new MediaQueryData(),
        child: MaterialApp(home: AppStateContainer(child: child)));
  }

  testWidgets('find loading indicator if list is empty',
          (WidgetTester tester) async {
        final collectionScreen = getRootWidget(
            child: CollectionScreen()); // Get a reference to a collectionScreen
        await tester.pumpWidget(
            collectionScreen); // Populate the test with CollectionScreen

        final collectionScreenFinder = find.byWidget(
            collectionScreen); // Find the collectionScreen within the test environment
        expect(collectionScreenFinder,
            findsOneWidget); // Ensure that it's found a widget

        final loadingIconFinder = find.descendant(
            of: collectionScreenFinder,
            matching: find.byType(
                CircularProgressIndicator)); // Find all the potential child elements within CollectionScreen
        expect(loadingIconFinder, findsOneWidget);
      });


  testWidgets('Successfully populates the list', (WidgetTester tester) async {
    final screen = CollectionScreen();
    final MockAppState container = MockAppState();

    await tester.pumpWidget(getRootWidget(child: screen));

    final CollectionScreenState state = tester.state(find.byWidget(screen));

    final List<Collection> list = List.of([Collection("KEY", "NAME")]);
    Completer completer = Completer();
    completer.complete();

    final AppState appState = AppState();
    appState.collections = list;

    when(container.collectionsFuture).thenAnswer((_) => completer.future);
    when(container.state).thenAnswer((_) => appState);

    await tester.pumpWidget(getRootWidget(child: state.getContent(container)));

    await tester.idle();
    await tester.pump();

    final listViewFinder = find.byType(ListView);
    expect(
        find.descendant(of: listViewFinder, matching: find.byType(ListButton)),
        findsOneWidget);
  });
}
