import 'package:ecmm427mobile/internal/app_state_container.dart';
import 'package:ecmm427mobile/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

//class MockAuth extends Mock implements BaseAuth {}

void main() {
  Widget makeTestableWidget({Widget child}){
    return MaterialApp(
      home: AppStateContainer(child: child),
    );
  }
  testWidgets('Login button disabled if no username or password',
          (WidgetTester tester) async {
        await tester.pumpWidget(
            makeTestableWidget(
              child: LogInScreen(false),
            )
        );

        Finder finder = find.byType(TextField);
        expect(finder, findsNWidgets(2));
        await tester.enterText(finder.at(0), '');
        await tester.enterText(finder.at(1), '');

        final raisedButton = find.byType(RaisedButton);
        expect(raisedButton, findsOneWidget);
        expect((raisedButton
            .evaluate()
            .single
            .widget as RaisedButton).color, Colors.transparent);
      });
}
