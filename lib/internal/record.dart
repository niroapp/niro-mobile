import 'package:ecmm427mobile/internal/scan.dart';

class Record {
  List<Scan> scans;
  String _name;
  String sampleID;
  String _waterVol;

  Record(Map record) {
    _name = record['Name'] as String ?? record['name'] as String;
    _waterVol = record['Water or SDS vol'].toString();
    sampleID = record['sample_id'] as String ?? record['reference'] as String;
    scans = List();
    this.addScan(Scan(record));
  }

  Record.fromValues(this._name, this._waterVol, this.sampleID, this.scans);

  addScan(Scan scan) {
    scans.add(scan);
  }

  String get name {
    if (_waterVol == 'n/a') {
      return _name;
    } else {
      return _name + '  (' + _waterVol + ' water vol)';
    }
  }

  @override
  String toString() {
    return '{"ID": "$sampleID", "Name": "$_name", "Scans": $scans';
  }

  String toJson() {
    return '{"name": "$_name", "reference": "$sampleID", "water_vol": "$_waterVol", "reads": [' +
        _scansToJson() + "]}";
  }

  String _scansToJson() {
    var string = "";
    var lindex = scans.length - 1;
    for (var i = 0; i < scans.length; i++) {
      string += scans[i].toJson();
      if (i == lindex) break;
      string += ",";
    }
    return string;
  }
}