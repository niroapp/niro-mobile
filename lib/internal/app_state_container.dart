import 'dart:convert';

import 'package:ecmm427mobile/internal/app_state.dart';
import 'package:ecmm427mobile/internal/collection.dart';
import 'package:ecmm427mobile/internal/niro_exceptions.dart';
import 'package:ecmm427mobile/internal/record.dart';
import 'package:ecmm427mobile/internal/result.dart';
import 'package:ecmm427mobile/internal/scan.dart';
import 'package:ecmm427mobile/io_helpers/exeter_cloud_helper.dart';
import 'package:ecmm427mobile/io_helpers/scio_cloud_helper.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum ActionType { COLLECTION, RECORD }

class AppStateContainer extends StatefulWidget {
  final AppState state;
  final Widget child;

  AppStateContainer({@required this.child, this.state});

  static AppStateContainerState of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(InheritedStateContainer)
    as InheritedStateContainer)
        .state;
  }

  @override
  State<StatefulWidget> createState() => AppStateContainerState();
}

class AppStateContainerState extends State<AppStateContainer> {
  AppState state = new AppState();

  ScioCloudHelper _scioCloudHelper = ScioCloudHelper();
  ExeterHandler _exeterCloudHelper = ExeterHandler();

  BuildContext ctx;

  Future collectionsFuture;
  Future recordsFuture;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Provide the new build context
    return InheritedStateContainer(state: this, child: widget.child);
  }

  Future getCollections(BuildContext context) async {
    this.ctx = context;
    if (await _getCachedCollectionList()) {
      return;
    }
    while (true) {
      try {
        var collections = await _scioCloudHelper.getCollections(context);
        _cacheCollections(collections);
        setState(() {
          state.collections = collections;
        });
        return;
      } on NiroException catch (e) {
        await _handleException(e, ActionType.COLLECTION);
      }
    }
  }

  Future getRecords(String collectionId) async {
    while (true) {
      try {
        var recordsFuture = _scioCloudHelper.getRecords(collectionId);
        setState(() {
          this.recordsFuture = recordsFuture;
        });

        var records = await recordsFuture;
        _cacheRecords(collectionId, records);

        return;
      } on NiroException catch (e) {
        await _handleException(e, ActionType.RECORD);
      }
    }
  }

  forceCollectionRefresh(bool pulledUp) {
    setState(() {
      state.collections = null;
    });
    collectionsFuture = getCollections(ctx);
  }

  initialCollectionInit(BuildContext context) {
    collectionsFuture = getCollections(context);
  }

  forceSpecificCollectionRefresh(bool pulledUp) {
    setState(() {
      state.records = null;
      recordsFuture = getRecords(this.state.selectedCollectionId);
    });
  }

  setCollection(BuildContext context, String collectionId) async {
    print("Setting collection to: " + collectionId);

    if (!await _getCachedCollection(collectionId)) {
      print('Collection not cached');
      setState(() {
        state.selectedCollectionId = collectionId;
        recordsFuture = getRecords(collectionId);
      });
    } else {
      print('Cached Collection found.');
      var records = await recordsFuture as List<Record>;
      setState(() {
        state.records = records;
      });
    }
    Navigator.pushNamed(context, "/RecordScreen");
  }

  Future<bool> _getCachedCollection(String collectionId) async {
    final sharedPrefs = await SharedPreferences.getInstance();

    final recordsString = sharedPrefs.getString(collectionId);
    if (recordsString == null) {
      return false;
    }
    var objectStringList = json.decode(recordsString);

    var future = _buildRecordListFromJsonList(objectStringList);
    setState(() {
      state.selectedCollectionId = collectionId;
      recordsFuture = future;
    });
    return true;
  }

  Future _emptyFuture() {
    return null;
  }

  submitForAnalysis(BuildContext context, List<Record> selectedRecords) async {
    if (!_exeterCloudHelper.isLoggedIn()) {
      await Navigator.of(context).pushNamed("/LoginScreenExeter");
    }

    setState(() {
      this.state.selectedRecords = selectedRecords;
      this.state.results = null;
      this.state.resultStream = _exeterCloudHelper.getResultStream();
    });
    Navigator.pushNamed(context, '/ResultScreen');
    print("Submitting ${selectedRecords.length} records for analysis.");
    await _exeterCloudHelper.uploadForAnalysis(selectedRecords);
  }

  scioLogOut(BuildContext context) {
    _scioCloudHelper.removeToken();
    Navigator.pushNamedAndRemoveUntil(
        context, "/LoginScreen", ModalRoute.withName('/'));
  }

  exeterLogOut(BuildContext context) {
    _exeterCloudHelper.removeToken();
    Navigator.of(context).pushNamedAndRemoveUntil('/', (Route route) => false);
  }

  Future<dynamic> scioLogIn(email, password) {
    var body = {'username': email, 'password': password};

    return _scioCloudHelper.handleLogin(body);
  }

  Future<dynamic> exeterLogIn(email, password) async {
    var body = {"email": email, "password": password};

    return _exeterCloudHelper.handleLogin(body);
  }

  void _handleException(NiroException e, ActionType causingMethodType) async {
    print(e.message);
    switch (e.code) {
      case ErrorCode.UNAUTHORISED:
        {
          // Unauthorised, attempt to log in again
          Navigator.of(ctx).pushReplacementNamed('/LoginScreen');
          return;
        }
      case ErrorCode.INVALID_USER_PASS:
        Navigator.of(ctx).pushReplacementNamed('/LoginScreen');
        return;
      default:
        {
          print(e);
        }
    }
  }

  void removeResults() {
    setState(() {
      this.state.resultStream = null;
    });
  }

  void _cacheRecords(String collectionID, List<Record> records) async {
    final sharedPrefs = await SharedPreferences.getInstance();
    sharedPrefs.setString(
        collectionID,
        json.encode(records, toEncodable: (dynamic record) {
          return (record as Record).toJson();
        }));
  }

  void _cacheCollections(List<Collection> collections) async {
    final sharedPrefs = await SharedPreferences.getInstance();
    sharedPrefs.setString(
        COLLECTIONS_KEY,
        json.encode(collections, toEncodable: (dynamic collection) {
          return (collection as Collection).toJson();
        }));
    print('Collections cached successfully');
  }

  Future<List<Record>> _buildRecordListFromJsonList(recordStringList) async {
    var records = List<Record>();

    for (String recordString in recordStringList) {
      Map record = json.decode(recordString) as Map;
      var recordObject = Record.fromValues(
          record['name'] as String,
          (record['water_vol'] as String) ?? "n/a",
          record['reference'] as String,
          _getScanListFromReads((record['reads'] as List<dynamic>).cast<
              Map<dynamic, dynamic>>()));
      records.add(recordObject);
    }
    return records;
  }

  List<Scan> _getScanListFromReads(List<Map> reads) {
    List<Scan> scans = List<Scan>();
    for (Map read in reads) {
      scans.add(Scan.fromValues(read['scanNumber'] as int,
          read['sampling_time'] as String, read['spectrum'] as List<dynamic>));
    }
    return scans;
  }

  getPreviousResults(BuildContext context) async {
    Stream<Result> resultStream =
    _exeterCloudHelper.getPreviousResultStream(context);

    setState(() {
      this.state.resultStream = resultStream;
    });
  }

  Future<bool> _getCachedCollectionList() async {
    final sharedPrefs = await SharedPreferences.getInstance();

    String js = sharedPrefs.getString(COLLECTIONS_KEY);
    print('No cached collections');
    if (js == null) {
      return false;
    }

    print(jsonDecode(js));
    print(jsonDecode(js).runtimeType);

    var temp = jsonDecode(js);

    List<String> collectionStrings = (jsonDecode(js) as List<dynamic>).cast<
        String>();
    List<Collection> collections = new List();
    for (String collectionString in collectionStrings) {
      Map<String, dynamic> collection = jsonDecode(collectionString) as Map<
          String,
          dynamic>;
      collections.add(Collection.fromJson(collection));
    }

    setState(() {
      this.state.collections = collections;
    });
    print('Loaded cached collections');
    return true;
  }

}

class InheritedStateContainer extends InheritedWidget {
  final AppStateContainerState state;

  InheritedStateContainer({
    Key key,
    @required this.state,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}
