abstract class NiroException implements Exception {
  ErrorCode code;
  String message;
  String additionalInformation;

  NiroException({this.additionalInformation: ""});

  @override
  String toString() {
    return message + "\nAdditional Information: " + additionalInformation;
  }
}

class InvalidUserPassException implements NiroException {
  ErrorCode code = ErrorCode.INVALID_USER_PASS;
  String message = "Invalid username or password supplied.";
  String additionalInformation = "";
}

class UnauthorisedException implements NiroException {
  ErrorCode code = ErrorCode.UNAUTHORISED;
  String message = "Unauthorised connection, perhaps the access token has expired.";
  String additionalInformation = "";
}

class UnknownException implements NiroException {
  ErrorCode code = ErrorCode.UNKNOWN;
  String message = "An unknown error occurred. Please refer to the developers for more information.";
  String additionalInformation = "";

  UnknownException({this.additionalInformation});
}

class JsonDecodeException implements NiroException {
  ErrorCode code = ErrorCode.JSON_DECODE;
  String message = "Unable to parse the given string to JSON";
  String additionalInformation;

  JsonDecodeException({this.additionalInformation: ""});
}

class NetworkException implements NiroException {
  ErrorCode code = ErrorCode.NETWORK_ERROR;
  String message = "There was an error when trying to connect to the NiRO server.";
  String additionalInformation;

  NetworkException({this.additionalInformation: ""});
}

enum ErrorCode {
  UNAUTHORISED,
  UNKNOWN,
  INVALID_USER_PASS,
  JSON_DECODE,
  NETWORK_ERROR
}