import 'package:ecmm427mobile/internal/collection.dart';
import 'package:ecmm427mobile/internal/record.dart';
import 'package:ecmm427mobile/internal/result.dart';

class AppState {
  String selectedCollectionId;
  List<Collection> collections;
  List<Record> records;

  List<Result> results;

  List<Record> selectedRecords;

  Stream<Result> resultStream;

  bool loggedIn = false;

  logOut() {
    loggedIn = false;
  }
}
