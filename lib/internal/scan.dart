class Scan {
  int scanNumber;
  String samplingTime;
  List<dynamic> spectrum;

  Scan(Map map) {
      this.scanNumber = map['id'] as int;
      this.samplingTime = map['sampling_time'] as String;
      this.spectrum = map['spectrum'] as List<dynamic>;
  }

  Scan.fromValues(this.scanNumber, this.samplingTime, this.spectrum);

  String toJson() {
    return '{"scanNumber": $scanNumber, "sampling_time": "$samplingTime", "spectrum": $spectrum}';
  }
}