class Collection {
  String id;
  String collectionName;

  Collection(this.id, this.collectionName);

  String toJson() {
    return '{"id": "${this.id}", "name": "$collectionName"}';
  }

  Collection.fromJson(Map<String, dynamic> json) {
    this.id = json['id'] as String;
    this.collectionName = json['name'] as String;
  }
}