class Result {
  String sampleID;
  Map<String, double> substanceResults;
  Map<String, double> moietyResults;
  String imageURL;
  String name = null;

  Result(Map result) {
    this.sampleID = result['reference'] as String;
    this.substanceResults =
        (result['type']['substances'] as Map<String, dynamic>)
            .cast<String, double>();
    this.moietyResults = (result['type']['moieties'] as Map<String, dynamic>)
        .cast<String, double>();
    this.imageURL = result['spectrum'] as String;
    this.name = result['name'] as String;
  }

  Result.fromList(String name, String sampleID, Map analysis, String imageURL) {
    this.name = name;
    this.sampleID = sampleID;
    this.imageURL = imageURL;
    this.substanceResults =
        (analysis['substances'] as Map<String, dynamic>).cast<String, double>();
    this.moietyResults =
        (analysis['moieties'] as Map<String, dynamic>).cast<String, double>();
  }

  @override
  bool operator ==(other) {
    if (other.runtimeType != Result) return false;
    Result result = other as Result;
    if (this.sampleID == result.sampleID) {
      if (this.name == null && result.name != null) this.name = result.name;
      if (this.imageURL == null && result.imageURL != null)
        this.imageURL = result.imageURL;
      return true;
    }
    return false;
  }
}
