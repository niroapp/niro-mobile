import 'package:ecmm427mobile/internal/app_state_container.dart';
import 'package:ecmm427mobile/internal/niro_exceptions.dart';
import 'package:ecmm427mobile/widgets/loading_indicator.dart';
import 'package:flutter/material.dart';

class LogInScreen extends StatefulWidget {
  State<StatefulWidget> createState() => LogInScreenState();

  final bool isForExeter;

  LogInScreen(this.isForExeter);
}

class LogInScreenState extends State<LogInScreen> {
  String _username;
  String _password;

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool _invalidUsernameOrPassword = false;

  bool _loading = false;

  LogInScreenState() {
    _usernameController.addListener(() {
      _username =
      _usernameController.text.isEmpty ? "" : _usernameController.text;
    });
    _passwordController.addListener(() {
      _password =
      _passwordController.text.isEmpty ? "" : _passwordController.text;
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override //将 flutter 插入到窗口部件树中时， 它会调用窗口部件的build方法
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlueAccent,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: _getContentBody(context),
        ),
      ),
    );
  }

  Widget _getContentBody(BuildContext context) {
    if (_loading) {
      return LoadingIndicator();
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
            this.widget.isForExeter
                ? "Log in to the Exeter Cloud"
                : "Log in to SCiO Lab",
            style: TextStyle(fontSize: 20.0)),
        _userPassError(),
        TextField(
          controller: _usernameController,
          decoration: InputDecoration(hintText: "Username"),
          keyboardType: TextInputType.emailAddress,
        ),
        TextField(
          controller: _passwordController,
          decoration: InputDecoration(
            hintText: "Password",
          ),
          obscureText: true,
        ),
        RaisedButton(
            child: Text("SUBMIT"),
            color: _isButtonEnabled() ? Colors.redAccent : Colors.transparent,
            onPressed: () async {
              if (_isButtonEnabled()) {
                setState(() {
                  _loading = true;
                });
                try {
                  if (this.widget.isForExeter) {
                    await AppStateContainer.of(context)
                        .exeterLogIn(_username, _password);
                    print('Logged in to NiRO');
                    Navigator.of(context).pop();
                    return;
                  } else {
                    await AppStateContainer.of(context)
                        .scioLogIn(_username, _password);
                    print('Logged in to Scio');
                    Navigator.of(context).pop();
                    return;
                  }
                } on InvalidUserPassException {
                  setState(() {
                    _loading = false;
                    _invalidUsernameOrPassword = true;
                  });
                }
              }
            })
      ],
    );
  }

  bool _isButtonEnabled() {
    bool enabled = true;
    if (_usernameController.text.isEmpty ||
        !_usernameController.text.contains('@')) {
      enabled = false;
    }
    if (_passwordController.text.isEmpty) {
      enabled = false;
    }
    return enabled;
  }

  Widget _userPassError() {
    if (this._invalidUsernameOrPassword) {
      return Text("Invalid username or password supplied.",
          style: TextStyle(color: Colors.red));
    }
    // Must return a widget
    return Container();
  }
}
