import 'package:flutter/material.dart';

class SettingsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new SettingsScreenState();
  }
}

class SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Settings')),
        body: Center(
          child: ListView(
            children: <Widget>[
              Center(
                  child: MaterialButton(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text('Open Source Libraries',
                          style:
                          TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.bold)),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/LibraryScreen');
                    },
                  ))
            ],
          ),
        ));
  }
}
