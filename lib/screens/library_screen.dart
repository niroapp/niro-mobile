import 'dart:convert';

import 'package:ecmm427mobile/widgets/niro_app_bar/niro_app_bar.dart';
import 'package:flutter/material.dart';

class LibraryScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NiroAppBar(
        title: Text('Open Source Libraries'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FutureBuilder(
          future: _getLibraries(context),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasError) {
              print(snapshot.error);
              // TODO: Handle this better
            }
            switch (snapshot.connectionState) {
              case ConnectionState.done:
                return Column(children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        'NiRO uses lots of helpful open source libraries, of which we are grateful for.',
                        style: TextStyle(
                            fontSize: 28.0, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center),
                  ),
                  Divider(),
                  Expanded(
                    child: ListView.builder(
                        itemCount: (snapshot.data as List).length,
                        //(snapshot.data as List).length,
                        itemBuilder: (BuildContext context, int index) {
                          final list = snapshot.data as List;
                          return Align(
                            child: Column(
                              children: <Widget>[
                                Align(
                                  child: new Text(list[index]['name'] as String,
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.left),
                                  alignment: Alignment.centerLeft,
                                ),
                                Align(
                                  child: new Text(
                                      list[index]['author'] as String,
                                      style: TextStyle(fontSize: 18.0),
                                      textAlign: TextAlign.left),
                                  alignment: Alignment.centerLeft,
                                ),
                                Align(
                                  child: new Text(
                                      (list[index]['type'] as String) + '\n',
                                      style: TextStyle(fontSize: 16.0),
                                      textAlign: TextAlign.left),
                                  alignment: Alignment.centerLeft,
                                ),
                                Align(
                                  child: new Text(
                                      list[index]['license'] as String,
                                      style: TextStyle(fontSize: 14.0),
                                      textAlign: TextAlign.justify),
                                  alignment: Alignment.centerLeft,
                                ),
                                Divider()
                              ],
                            ),
                            alignment: Alignment.centerLeft,
                          );
                        }),
                  )
                ]);
              default:
                return CircularProgressIndicator();
            }
          },
        ),
      ),
    );
  }

  Future<List<dynamic>> _getLibraries(BuildContext context) async {
    final String = await DefaultAssetBundle.of(context)
        .loadString("assets/libraries.json");
    final jsonFile = json.decode(String);
    return jsonFile['libraries'] as List<dynamic>;
  }
}
