import 'package:ecmm427mobile/internal/result.dart';
import 'package:ecmm427mobile/io_helpers/exeter_cloud_helper.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new DetailScreenState();
  }
}

class DetailScreenState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    final Result result = ModalRoute
        .of(context)
        .settings
        .arguments as Result;
    print(result.toString());
    return Scaffold(
        appBar: AppBar(title: Text(result.name)),
        body: Padding(
            padding: const EdgeInsets.all(24.0),
            child: Center(
                child: ListView(children: <Widget>[
                  _getMoieties(result),
                  _getSubstances(result),
                  _getImage(result),
                ]))));
  }

  Widget _getMoieties(Result result) {
    List<Widget> widgets = List();
    if (result.moietyResults == null || result.moietyResults.isEmpty) {
      return Container(height: 0, width: 0);
    }

    widgets.add(Row(
      children: <Widget>[
        Text(
          'Moieties',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
          textAlign: TextAlign.left,
        ),
      ],
    ));

    for (String key in result.moietyResults.keys) {
      widgets.add(Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          children: <Widget>[
            Text(key, textAlign: TextAlign.left),
            Spacer(),
            Text(result.moietyResults[key].toStringAsFixed(3),
                textAlign: TextAlign.right)
          ],
        ),
      ));
    }
    widgets.add(Container(height: 32.0));
    return Column(children: widgets);
  }

  Widget _getSubstances(Result result) {
    List<Widget> widgets = List();

    if (result.substanceResults == null || result.substanceResults.isEmpty) {
      return Container(height: 0, width: 0);
    }

    widgets.add(Row(
      children: <Widget>[
        Text(
          'Substances',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
          textAlign: TextAlign.left,
        ),
      ],
    ));

    for (String key in result.substanceResults.keys) {
      widgets.add(Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          children: <Widget>[
            Text(key, textAlign: TextAlign.left),
            Spacer(),
            Text(result.substanceResults[key].toStringAsFixed(3),
                textAlign: TextAlign.right)
          ],
        ),
      ));
    }
    widgets.add(Container(height: 32.0));
    return Column(children: widgets);
  }

  Widget _getImage(Result result) {
    final url = ExeterHandler.buildImageUrl(result.imageURL);
    return GestureDetector(
        onTap: () {
          Map<String, String> args = Map();
          args['imageURL'] = ExeterHandler.buildImageUrl(result.imageURL);
          args['name'] = result.name; // TODO: Change to name
          Navigator.pushNamed(context, '/FullscreenImage', arguments: args);
        },
        child: Container(
            decoration:
            BoxDecoration(border: Border.all()),
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Image.network(url),
            )));
  }
}
