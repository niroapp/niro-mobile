import 'package:flutter/material.dart';

class FullscreenImageScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Map args = ModalRoute.of(context).settings.arguments as Map;

    final String name = args['name'] as String;
    final String url = args['imageURL'] as String;

    return Scaffold(
        appBar: AppBar(title: Text(name)),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: RotatedBox(
              child: Image.network(url, fit: BoxFit.fill), quarterTurns: 1),
        ));
  }
}
