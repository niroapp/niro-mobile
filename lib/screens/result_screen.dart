import 'package:ecmm427mobile/internal/app_state_container.dart';
import 'package:ecmm427mobile/internal/result.dart';
import 'package:ecmm427mobile/widgets/list_button.dart';
import 'package:ecmm427mobile/widgets/loading_indicator.dart';
import 'package:ecmm427mobile/widgets/niro_app_bar/niro_app_bar.dart';
import 'package:flutter/material.dart';

class ResultScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new ResultScreenState();
  }
}

class ResultScreenState extends State<ResultScreen> {
  List<Result> resultList = List();

  @override
  Widget build(BuildContext context) {
    final root = AppStateContainer.of(context);
    return WillPopScope(
      child: new Scaffold(
        appBar: NiroAppBar(
          title: Text("Results"),
        ),
        body: SafeArea(child: Center(child: _getContent(root))),
      ),
      onWillPop: () async {
        root.removeResults();
        return true;
      },
    );
  }

  Widget _getContent(AppStateContainerState root) {
    return StreamBuilder(
      stream: root.state.resultStream,
      builder: (BuildContext context, AsyncSnapshot<Result> snapshot) {
        if (snapshot.hasError) {
          print(snapshot.error);
          return Center(
              child: Text(
                'There was an error evaluating results!\nCome back later or try restarting the application.',
                textAlign: TextAlign.center,
              ));
        } else {
          switch (snapshot.connectionState) {
            case ConnectionState.done:
            case ConnectionState.active:
              print(resultList);
              if (!resultList.contains(snapshot.data)) {
                resultList.add(snapshot.data);
              }
              return ListView.builder(
                  itemCount: resultList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: _buildItem(
                          root, resultList[index], index, context),
                    );
                  });
            case ConnectionState.waiting:
            default:
              return LoadingIndicator();

          }
        }
      },
    );
  }

  Widget _buildItem(AppStateContainerState root, Result result, int index,
      BuildContext context) {

    return ListButton(
        index, result.sampleID, (result.name ?? result.sampleID) as String,
        false, () {
      Navigator.pushNamed(context, '/DetailScreen', arguments: result);
    });
  }
}
