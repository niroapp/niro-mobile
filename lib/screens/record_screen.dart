import 'package:ecmm427mobile/internal/app_state_container.dart';
import 'package:ecmm427mobile/internal/record.dart';
import 'package:ecmm427mobile/widgets/list_button.dart';
import 'package:ecmm427mobile/widgets/niro_app_bar/niro_app_bar.dart';
import 'package:flutter/material.dart';

class RecordScreen extends StatefulWidget {
  final String id;

  @override
  State<StatefulWidget> createState() => RecordScreenState();

  RecordScreen({this.id});
}

class RecordScreenState extends State<RecordScreen> {
  final selectedRecords = List<Record>();

  Widget buildListItem(AppStateContainerState root, int index) {
    Record record = root.state.records[index];
    return ListButton(index, record.sampleID, record.name, true, () {
      if (selectedRecords.remove(record)) {
        return;
      }
      selectedRecords.add(record);
    });
  }

  Widget _getListContent(AppStateContainerState root, List<Record> data) {
    print(data);
    if (data.length == 0) {
      return Center(
          child: Text(
            'The chosen collection is empty.',
            style: TextStyle(fontSize: 20.0),
          ));
    }
    return Column(children: [
      Flexible(
            child: ListView.builder(
                itemCount: data.length,
                itemBuilder: (context, index) {
                  return buildListItem(root, index);
                }),
      ),
      Row(
        children: <Widget>[
          Spacer(),
          RaisedButton(
            child: Text('REFRESH'),
            color: Colors.blue,
            onPressed: () {
              root.forceSpecificCollectionRefresh(true);
            },
          ),
          Spacer(),
          RaisedButton(
              child: Text("SUBMIT"),
              color: Colors.blue,
              onPressed: () {
                root.submitForAnalysis(context, selectedRecords);
              }),
          Spacer()
        ],
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
      )
    ]);
  }

  Widget getContent(AppStateContainerState root) {
    return Column(
      children: <Widget>[
        Expanded(
            child: FutureBuilder(
              future: root.recordsFuture,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  print(snapshot.error);
                  return Text("There was an error with the application");
                } else if (snapshot.connectionState == ConnectionState.done) {
                  return _getListContent(root, snapshot.data as List<Record>);
                } else {
                  return Container(
                    child: Column(
                      children: <Widget>[
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 12.0),
                          child: Text("This may take some time.."),
                        ),
                        SizedBox(
                          height: 150.0,
                          width: 150.0,
                          child: CircularProgressIndicator(),
                        ),
                        Spacer()
                      ],
                    ),
                  );
                }
              },
            ))
      ],
    );
  }


  @override
  Widget build(BuildContext context) {
    final root = AppStateContainer.of(context);
    return new Scaffold(
      appBar: NiroAppBar(
        title: Text("Choose a Record"),
      ),
      body: SafeArea(child: Center(child: getContent(root))),
    );
  }
}
