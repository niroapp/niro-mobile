import 'package:ecmm427mobile/internal/app_state_container.dart';
import 'package:ecmm427mobile/widgets/list_button.dart';
import 'package:ecmm427mobile/widgets/niro_app_bar/niro_app_bar.dart';
import 'package:flutter/material.dart';

class CollectionScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new CollectionScreenState();
  }
}

class CollectionScreenState extends State<CollectionScreen> {
  Widget buildListItem(int index, String key, String value) {
    return ListButton(
      index,
      key,
      value,
      false,
          () {
        AppStateContainer.of(context).setCollection(context, key);
      },
    );
  }

  Widget getContent(AppStateContainerState root) {
    return Column(children: <Widget>[
      Expanded(
          child: FutureBuilder(
              future: root.collectionsFuture,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  print(snapshot.error);
                  return Center(
                      child: Text(
                          "Oh dear! It looks like something went wrong. \n"
                              "Please get in touch with the developers or restart the app."));
                } else if (snapshot.connectionState == ConnectionState.done) {
                  return Container(
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: ListView.builder(
                            itemCount: root.state.collections.length,
                            itemBuilder: (context, index) {
                              var item = root.state.collections[index];
                              return buildListItem(
                                  index, item.id, item.collectionName);
                            },
                          ),
                        ),
                        RaisedButton(
                          child: Text('REFRESH'),
                          onPressed: () {
                            root.forceCollectionRefresh(true);
                          },
                          color: Colors.blue,
                        ),
                      ],
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                    ),
                  );
                } else {
                  print(snapshot.connectionState);
                  return Container(
                    child: Column(
                      children: <Widget>[
                        Spacer(),
                        SizedBox(
                          height: 150.0,
                          width: 150.0,
                          child: CircularProgressIndicator(),
                        ),
                        Spacer()
                      ],
                    ),
                  );
                }
              }))
    ]);
  }

  AppStateContainerState getRoot(BuildContext context) {
    return AppStateContainer.of(context);
  }

  @override
  Widget build(BuildContext context) {
//    final root = AppStateContainer.of(context);
    final root = getRoot(context);
    return new Scaffold(
      appBar: NiroAppBar(
        title: Text("Choose a Collection"),
      ),
      body: SafeArea(
          child: Center(
//          child: Container()
              child: getContent(root))),
    );
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    final root = AppStateContainer.of(context);
    if (root.collectionsFuture == null) {
      root.initialCollectionInit(context);
    }
  }
}
