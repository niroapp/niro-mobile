import 'package:ecmm427mobile/internal/app_state_container.dart';
import 'package:ecmm427mobile/internal/niro_exceptions.dart';
import 'package:ecmm427mobile/widgets/niro_app_bar/niro_app_bar.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: NiroAppBar(title: Text('NiRO')),
      body: Padding(
        padding: EdgeInsets.all(24.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Spacer(),
            Center(child: Text('Welcome to', style: TextStyle(fontSize: 28.0))),
            Center(child: Text('NiRO', style: TextStyle(fontSize: 36.0))),
            Spacer(),
            Expanded(
              child: RaisedButton(
                  color: Colors.blue,
                  child: Text('Submit a new scan',
                      style: TextStyle(fontSize: 28.0)),
                  onPressed: () {
                    print('Submit Scan');
                    Navigator.of(context).pushNamed('/CollectionScreen');
                  },
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0))),
            ),
            Spacer(),
            Expanded(
              child: RaisedButton(
                  color: Colors.blue,
                  child: Text('View previous results',
                      style: TextStyle(fontSize: 28.0)),
                  onPressed: () async {
                    print('Previous results');
                    try {
                      await AppStateContainer.of(context).getPreviousResults(
                          context);
                    } on UnauthorisedException catch (e) {
                      print('CAUGHT' + e.toString());
                      await Navigator.of(context).pushNamed(
                          '/LoginScreenExeter');
                      await AppStateContainer.of(context).getPreviousResults(
                          context);
                    }
                    Navigator.of(context).pushNamed('/ResultScreen');
                  },
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0))),
            ),
            Spacer(),
            Spacer()
          ],
        ),
      ),
    );
  }
}
