import 'package:ecmm427mobile/internal/app_state_container.dart';
import 'package:ecmm427mobile/screens/collection_screen.dart';
import 'package:ecmm427mobile/screens/detail_screen.dart';
import 'package:ecmm427mobile/screens/fullscreen_image.dart';
import 'package:ecmm427mobile/screens/home_screen.dart';
import 'package:ecmm427mobile/screens/library_screen.dart';
import 'package:ecmm427mobile/screens/login_screen.dart';
import 'package:ecmm427mobile/screens/record_screen.dart';
import 'package:ecmm427mobile/screens/result_screen.dart';
import 'package:ecmm427mobile/screens/settings_screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Builder(builder: (BuildContext context) {
      return AppStateContainer(
          child: Builder(
            builder: (context) => MaterialApp(
                theme: ThemeData(
                  primarySwatch: Colors.blue,
                ),
                initialRoute: "/",
                routes: {
                  "/": (context) => HomeScreen(),
                  "/CollectionScreen": (context) => CollectionScreen(),
                  "/RecordScreen": (context) => RecordScreen(),
                  "/LoginScreen": (context) => LogInScreen(false),
                  "/LoginScreenExeter": (context) => LogInScreen(true),
                  "/SettingsScreen": (context) => SettingsScreen(),
                  "/ResultScreen": (context) => ResultScreen(),
                  '/LibraryScreen': (context) => LibraryScreen(),
                  '/DetailScreen': (context) => DetailScreen(),
                  '/FullscreenImage': (context) => FullscreenImageScreen(),
                }
            ),
          )
      );
    });
  }
}
