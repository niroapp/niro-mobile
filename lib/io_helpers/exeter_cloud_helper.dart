import 'dart:convert';
import 'dart:io';

import 'package:ecmm427mobile/internal/niro_exceptions.dart';
import 'package:ecmm427mobile/internal/record.dart';
import 'package:ecmm427mobile/internal/result.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

const MAIN_ADDR = "://176.58.124.102:8000";
//const MAIN_ADDR = "://192.168.43.60:8000";

const UPLOAD_API_URL = 'http' + MAIN_ADDR + "/api/samples/analyse/";
//const AUTH_TOKEN = "f5bf24ce513dd73b2cd91af362307c8f5db3a7dc";

const SOCKET_ADDRESS = 'ws' + MAIN_ADDR + '/ws/monitor/';

const PREVIOUS_RESULTS_URL = 'http' + MAIN_ADDR + "/api/samples/";
const AUTH_ENDPOINT = 'http' + MAIN_ADDR + "/api/auth/token/";


class ExeterHandler {
  // Singleton pattern
  static final ExeterHandler _cloudHandler = new ExeterHandler._internal();

  String _authToken = null;


  factory ExeterHandler() {
    return _cloudHandler;
  }

  static String buildImageUrl(String prefix) {
    String url;
    if (prefix.startsWith('http')) {
      url = 'http' + MAIN_ADDR + prefix.substring(prefix.indexOf('/media/'));
    } else {
      url = 'http' + MAIN_ADDR + prefix;
    }
    return url;
  }


  ExeterHandler._internal();

  uploadForAnalysis(List<Record> records) async {
    if (_authToken == null) {
      throw UnauthorisedException();
    }
    var request = new http.Request('POST', Uri.parse(UPLOAD_API_URL));
    request.headers[HttpHeaders.contentTypeHeader] = 'application/json';
    request.headers[HttpHeaders.authorizationHeader] = 'JWT ' + _authToken;
    request.body = _toJson(records);
    print(request.body);

    var response = await http.Client().send(request);
    if (response.statusCode != 200) {
      throw NetworkException(
          additionalInformation: await response.stream.bytesToString());
    }
  }

  String _toJson(List<Record> records) {
    String string = '{ "samples": [';
    var lindex = records.length - 1;
    for (var i = 0; i < records.length; i++) {
      string += records[i].toJson();
      if (i == lindex) break;
      string += ",";
    }
    string += ']}';
    return string;
  }

  Stream<Result> getResultStream() async* {
    if (_authToken == null) {
      throw UnauthorisedException();
    }
    Map<String, String> headers = new Map();
    headers['Authorization'] = 'JWT ' + _authToken;
    headers['Origin'] = 'http://192.168.0.1:8000';
    print(headers);

    try {
      WebSocket ws = await WebSocket.connect(SOCKET_ADDRESS, headers: headers);

      await for (dynamic packet in ws) {
        print('PACKET' + packet.toString());
        Map jsonMap = json.decode(packet.toString()) as Map;
        Result result = Result(jsonMap);
        yield result;
      }
    } catch (e) {
      print(e);
    }
  }

  Stream<Result> getPreviousResultStream(BuildContext context,
      {url: PREVIOUS_RESULTS_URL}) async* {
    if (_authToken == null) {
      await Navigator.of(context).pushNamed('/LoginScreenExeter');
    }
    Map<String, String> headers = new Map();
    headers['Authorization'] = 'JWT ' + _authToken;
    var body = (await http.get(url, headers: headers)).body;
    Map<String, dynamic> js = json.decode(body) as Map<String, dynamic>;

    if (js.containsKey('detail') && js['detail'] == 'Signature has expired.') {
      throw UnauthorisedException();
    }

    List<dynamic> results = js['results'] as List;
    for (var result in results) {
      Result res = Result.fromList(
          result['name'] as String, result['reference'] as String,
          result['analysis'] as Map, result['spectrum'] as String);
      yield res;
    }

    if (js.containsKey('next') && js['next'] != null) {
      // Load the paginated results
      var url = PREVIOUS_RESULTS_URL + '?' +
          js['next'].toString().split('?')[1];
      await for (var result in getPreviousResultStream(context, url: url)) {
        yield result;
      }
    }
  }

  handleLogin(body) async {
    Map<String, String> headers = new Map();
    headers[HttpHeaders.contentTypeHeader] = 'application/json';
    print(jsonEncode(body));
    http.Response response = await http.post(
        AUTH_ENDPOINT, headers: headers, body: jsonEncode(body));
    if (response.statusCode != 200) {
      // TODO: Change this to be a better reflection of what's going on!
      throw InvalidUserPassException();
    }
    _authToken = jsonDecode(response.body)['token'] as String;
    return;
  }

  bool isLoggedIn() {
    return _authToken != null;
  }

  removeToken() {
    _authToken = null;
  }
}
