import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:ecmm427mobile/internal/collection.dart';
import 'package:ecmm427mobile/internal/niro_exceptions.dart';
import 'package:ecmm427mobile/internal/record.dart';
import 'package:ecmm427mobile/internal/scan.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

const LOGIN_TOKEN_KEY = "LOGIN_TOKEN";
const REDIRECT_URI =
    "https%3A%2F%2Fsciolab.consumerphysics.com%2Flogin_handle.html";
const LOGIN_ENDPOINT = "https://auth.consumerphysics.com/oauth/login";
const COLLECTIONS_ENDPOINT = "https://api.consumerphysics.com/v1/collections";

const COLLECTIONS_KEY = "COLLECTIONS";

const CHECK_ENDPOINT = "https://api.consumerphysics.com/v1/jobs/";


class ScioCloudHelper {
  SharedPreferences _sharedPrefs;
  String _token = null;
  BuildContext _context;
  bool _isNotInitialised = true;

  // Generate two different HttpClient to navigate the nasty header extraction / population
  Dio _dio = Dio();
  HttpClient _client = HttpClient();

  _initialise() async {
    _sharedPrefs = await SharedPreferences.getInstance();
    _isNotInitialised = false;
  }

  Future<List<Collection>> getCollections(BuildContext context) async {
    if (_isNotInitialised) await _initialise();

    if (_token == null) {
      loadToken();
      if (_token == null) {
        await Navigator.of(context).pushNamed('/LoginScreen');
      }
    }

    var collectionList = _getCollectionList();
    return collectionList;
  }

  Future<List<Record>> getRecords(String collectionId) async {
    if (_isNotInitialised) await _initialise();

    if (_token == null) {
      loadToken();
      if (_token == null) {
        throw UnauthorisedException();
      }
    }

    return _getRecordList(collectionId);
  }

  Future<List<Record>> _getRecordList(String collectionId) async {
    var json = await _getUrlAsJson(COLLECTIONS_ENDPOINT + "/" + collectionId + "/download?format=json");

    // TODO: Handle [] called on null
    String downloadUrl = json['collection_download_data']['download_url'] as String;
    String jobId = json['collection_download_data']['job_id'] as String;

    await _jobCompletion(jobId);

    var jsonMap = await _getUrlAsJson(downloadUrl, useAccessToken: false);

    var records = List<Record>();
    for (Map record in jsonMap['records']) {
      var index = records
          .map((Record record) {
            return record.sampleID;
          })
          .toList()
          .indexOf(record['sample_id'] as String);
      if (index != -1) {
        // We've already added this Record to the list
        records[index].addScan(Scan(record));
      } else {
        records.add(Record(record));
      }
    }
    return records;
  }

  Future _jobCompletion(String jobId) async {
    String status = 'starting';
    while (status != "completed") {
      print('Current status of job [$jobId] is $status');
      var json = await _getUrlAsJson(CHECK_ENDPOINT + jobId);
      print(json);
      status = json['status'] as String;
      {
        //TODO: Handle failure
      }
    }
  }

  Future handleLogin(var body) async {
    //  Build the login url
    var loginUrl =
        "${LOGIN_ENDPOINT}?response_type=token&client_id=web_client&redirect_uri=" +
            REDIRECT_URI +
            "&scope=sdk+collection+user_profile+user_edit_profile+change_pass+third_party_app&mode=thin";

    // Log in to the server
    var response = await _dio.post(loginUrl,
        data: body,
        options: Options(
          // Dio doesn't automatically do redirects for post requests
            validateStatus: (status) {
              return status < 500;
            },
            followRedirects: true));
    _checkForErrors(response);
    var redirectUrl = response.headers['location'][0];

    // Authenticate with the server to access token
    var request = await _client.getUrl(Uri.parse(redirectUrl));
    request.headers.add("referer", redirectUrl);
    var finalResponse = await request.close();

    String address;
    if (finalResponse.redirects.length < 1) {
      throw UnknownException(additionalInformation: "Connection scheme has changed!");
    } else {
      address = finalResponse.redirects.last.location.toString();
    }

    _token = _extractToken(address);
    _saveToken();
  }

  String _extractToken(String address) {
    address = address.substring(address.indexOf("#") + 1);
    var list = address.split("&");
    Map keyVals = {};

    for (String item in list) {
      var keyVal = item.split("=");
      keyVals[keyVal[0]] = keyVal[1];
    }

    return keyVals['access_token'] as String;
  }

  void loadToken() {
    _token = _sharedPrefs.getString(LOGIN_TOKEN_KEY) ?? null;
    if (null != _token) {
      print("Loaded a token: " + _token);
    }
  }

  void _checkForErrors(Response response) {
    if (response.statusCode == 401) {
      throw InvalidUserPassException();
    }
    if (response.data is Map) {
      if (response.data['error_type'] == "InvalidUsage") throw InvalidUserPassException();
    }
  }

  Future<List<Collection>> _getCollectionList() async {
    var json = await _getUrlAsJson(COLLECTIONS_ENDPOINT);
    var collections = List<Collection>();
    for (Map collection in json['collections']) {
      collections.add(
          Collection(collection['id'] as String, collection['name'] as String));
    }
    return collections;
  }

  dynamic _getUrlAsJson(String url, {bool useAccessToken: true}) async {
    String response;
    if (useAccessToken) {
      response = await _getUrl(url);
    } else {
      print('Attempting to get url: $url');

      var request = new http.Request('GET', Uri.parse(url));
      http.StreamedResponse res = await http.Client().send(request);
      response = await res.stream.bytesToString();
//
//      Response res = await _dio.get(url);
//      response = res.data.toString();
    }
    print(response);
    return parseJson(response);
  }

  Future<String> _getUrl(String url) async {
    var request = new http.Request('GET', Uri.parse(url));
    request.headers[HttpHeaders.contentTypeHeader] = 'application/json';
    request.headers[HttpHeaders.authorizationHeader] = "Bearer " + _token;
    var response = await http.Client().send(request);
    return await response.stream.bytesToString();
  }

  Map<String, dynamic> parseJson(String jsonString) {
    try {
      return jsonDecode(jsonString) as Map<String, dynamic>;
    } on FormatException {
      throw JsonDecodeException();
    }
  }

  void removeToken() async {
    if (_isNotInitialised) {
      await _initialise();
    }
    _sharedPrefs.remove(LOGIN_TOKEN_KEY);
    _token = null;
  }

  void _saveToken() async {
    if (_isNotInitialised) {
      await _initialise();
    }
    _sharedPrefs.setString(LOGIN_TOKEN_KEY, _token);
  }
}
