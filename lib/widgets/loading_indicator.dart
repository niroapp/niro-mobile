import 'package:flutter/material.dart';

class LoadingIndicator extends StatelessWidget {
  const LoadingIndicator({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(bottom: 12.0),
            child: Text("This may take some time.."),
          ),
          SizedBox(
            height: 150.0,
            width: 150.0,
            child: CircularProgressIndicator(),
          ),
          Spacer()
        ],
      ),
    );
  }
}
