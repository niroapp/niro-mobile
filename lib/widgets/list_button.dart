import 'package:flutter/material.dart';

class ListButton extends StatefulWidget {
  final Function callback;
  final String text;
  bool isSelectable;
  Key key;
  Color _defaultColor;

  @override
  State<StatefulWidget> createState() => ListButtonState();

  ListButton(int index, String key, this.text, this.isSelectable,
      this.callback) {
    this.key = Key(key);
    if (index % 2 == 0) {
      _defaultColor = Color.fromARGB(0, 229, 229, 229);
    } else {
      _defaultColor = Color.fromARGB(128, 229, 229, 229);
    }
  }
}

class ListButtonState extends State<ListButton> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      color: _getColor(),
      key: this.widget.key,
      child: new Text(this.widget.text, style: TextStyle(fontSize: 18.0)),
      onPressed: _handlePress,
    );
  }

  Color _getColor() {
    if (selected) {
      return Colors.blue;
    }
    return this.widget._defaultColor;
  }

  void _handlePress() {
    if (this.widget.isSelectable) {
      setState(() {
        selected = !selected;
      });
    }
    this.widget.callback();
  }
}