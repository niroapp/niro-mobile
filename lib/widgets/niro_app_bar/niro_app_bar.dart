import 'package:ecmm427mobile/internal/app_state_container.dart';
import 'package:ecmm427mobile/screens/settings_screen.dart';
import 'package:ecmm427mobile/widgets/niro_app_bar/menu_button_type.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NiroAppBar extends AppBar {
  static AppBarBuilder _builder;

  NiroAppBar({Key key, Widget title})
      : super(key: key, title: title, actions: <Widget>[
    PopupMenuButton(
        onSelected: _onSelected,
        itemBuilder: (BuildContext context) {
          _builder = AppBarBuilder(context: context);
          return _builder.build();
        })
  ]);

  static void _onSelected(value) {
    _builder.onSelected(value as MenuButtonType);
  }
}

class AppBarBuilder {
  BuildContext context;

  AppBarBuilder({this.context});

  List<PopupMenuItem> build() {
    return [
      PopupMenuItem(child: Text("Settings"), value: MenuButtonType.SETTINGS),
      PopupMenuItem(
          child: Text("Log out of SCiO"), value: MenuButtonType.SCIO_LOGOUT),
      PopupMenuItem(
          child: Text("Log out of NiRO"), value: MenuButtonType.NIRO_LOGOUT),
      PopupMenuItem(
          child: Text("Clear cached items"), value: MenuButtonType.CLEAR_CACHE),
    ];
  }

  void onSelected(MenuButtonType menuButtonType) async {
    final root = AppStateContainer.of(context);

    switch (menuButtonType) {
      case MenuButtonType.SETTINGS:
        {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => SettingsScreen()));
          break;
        }
      case MenuButtonType.SCIO_LOGOUT:
        root.scioLogOut(context);
        break;
      case MenuButtonType.NIRO_LOGOUT:
        root.exeterLogOut(context);
        break;
      case MenuButtonType.CLEAR_CACHE:
        var sharedPrefs = await SharedPreferences.getInstance();
        var result = await sharedPrefs.clear();
        print('Clearing cache: $result');
        _showWarning(context);
    }
  }

  _showWarning(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Clearing Cache'),
            content: Text(
                "The app will need to be restarted for the changes to take effect."),
            actions: <Widget>[
              RaisedButton(
                  child: Text('OK', style: TextStyle(color: Colors.black)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  color: Colors.blue)
            ],
          );
        });
  }
}
